function showImg(e) {
	document.getElementById("myModal").style.display = "block";
	document.getElementById("myImg").src = e.src;
	document.getElementById("myCaption").innerHTML = e.alt;
}

function checkInfo(form){
	var data = "คุณ" + form.firstName.value + " " + form.lastName.value + " ";
	data += "คุณต้องการจองที่พักหรือไม่?\n(ระบบจะทำการตัดยอดจากหมายเลขบัตรอัตโนมัติ)";

	if (confirm(data)) {
		alert("คุณได้ทำการจองที่พักสำเร็จแล้ว !");
		return true;
	}else{
		return false;
	}
}